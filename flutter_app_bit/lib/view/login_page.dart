import 'package:flutter_app_bit/util/colors.dart';
import 'package:flutter_app_bit/util/resize.dart';
import 'package:flutter_app_bit/view/widgets/app_button.dart';
import 'package:flutter_app_bit/view/widgets/app_text_field.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_app_bit/util/firebaseController.dart' as firebaseController;
import '../util/colors.dart';
import '../util/styles.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();

}

class _LoginPageState extends State<LoginPage> {

  bool logButton = false;
  bool eyeButton = false;

  TextEditingController _textEditingControllerEmail = new TextEditingController();
  TextEditingController _textEditingControllerPassword = new TextEditingController();



  @override
  Widget build(BuildContext context) {
    var _Height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
        // resizeToAvoidBottomInset: false,
        body: Container(
      child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/img/backbit.png'),
                colorFilter:
                    ColorFilter.mode(Colors.white, BlendMode.softLight),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: colorBlanco.withAlpha(100),
          ),
          Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                    //top: 40,
                    top: ResizeH(_Height, 40),
                    left: 25,
                    right: 25),
                height: _Height * 0.5,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 15,
                    ),
                  ],
                  color: colorBlanco,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(150),
                    bottomRight: Radius.circular(130),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Welcome to",
                      style: GoogleFonts.signika(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: ResizeH(
                          _Height,
                          30,
                        ),
                      ),
                    ),
                    Image.asset("assets/img/backbit.png",
                        width: ResizeH(_Height, 200),
                        height: ResizeH(_Height, 10),
                        fit: BoxFit.contain),
                    Text(
                      'Please login to continue',
                      style: GoogleFonts.signika(
                        fontSize: ResizeH(_Height, 20),
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),

                    Align(
                      alignment: Alignment.center,
                      child: TextField(
                        controller: _textEditingControllerEmail,
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            hintText: 'Username',
                            icon: Icon(
                            Icons.person,
                            color: Colors.grey,
                          )),

                      ),
                    ),
                    SizedBox(
                      height: _Height * 0.02,
                    ),
                    Align(
                        alignment: Alignment.center,
                        child: TextField(
                          controller: _textEditingControllerPassword,
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            hintText: 'Password',
                            icon: Icon(
                              Icons.lock,
                              color: Colors.grey,
                            ),
                          ),
                          obscureText: true,
                        )),
                    Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Forgot password',
                        )),
                    SizedBox(
                      height: _Height * 0.01,
                    ),

                   Align(
                      alignment: Alignment.center,
                      child: AppButton(
                        text: 'LOG IN ',
                        onPressed: () {
                          setState(() {
                            logButton != logButton;
                            if(_textEditingControllerPassword.text.isEmpty || _textEditingControllerEmail.text.isEmpty){
                              print('no autenticado');
                            }else{
                               firebaseController.signIn(email: _textEditingControllerEmail.text, password: _textEditingControllerPassword.text);
                              firebaseController.stateFirebase(context);
                            }
                          });
                         // Navigator.pushReplacementNamed(context, '/news');
                        },
                      ),
                    ),
                    Expanded(
                      flex: 500,
                      child: SizedBox(),
                    )

                  ],
                ),
              ),
              Expanded(
                flex: 500,
                child: SizedBox(),
              ),

              /*Center(
                child: InkWell(
                  onTap: (){
                    print('Presionando');
                    setState(() {
                      logButton != logButton;
                      if(_textEditingControllerPassword.text.isEmpty || _textEditingControllerEmail.text.isEmpty){
                        print('no autenticado');
                      }else{
                        // ignore: unnecessary_statements
                        firebaseController.signIn(email: _textEditingControllerEmail.text, password: _textEditingControllerPassword.text);
                        firebaseController.stateFirebase(context);
                      }
                    });
                  },
                  hoverColor: Colors.red,
                  focusColor: Colors.red,
                ),
              ),*/


              Text(
                'OR',
                style: Styles.secondaryTextStyle,
              ),
              Expanded(
                flex: 1000,
                child: SizedBox(),
              ),
              AppButton(
                text: 'SING UP',
                onPressed: () {},
              ),
              Expanded(
                flex: 1000,
                child: SizedBox(),
              ),
            ],
          ),
        ],
      ),
    ));
  }
}
